'日期：2016-10-19
'版本：0.1
'根据段落生成算术加减题

Private Sub CommandButton1_Click()
  Dim MAX_NUMBER As Integer
  Dim i  As Integer, A As Integer, B As Integer, FU As Integer, preMaxNumber As Integer, preNumber As String, maxCount As Integer
  Dim p As Paragraph, out As String
  Dim styleExt As Boolean, haveZero As Boolean, canJT As Boolean
  MAX_NUMBER = CLng(TextBox1.Text)
  styleExt = CheckBox1.Value
  haveZero = CheckBox2.Value '禁0
  canJT = CheckBox3.Value
  If MAX_NUMBER = 0 Then
    TextBox1.SetFocus
    TextBox1.SelText = TextBox1.Text
    Exit Sub
  End If
  If canJT Then '禁止进退位等同于10以内加减法
    preMaxNumber = MAX_NUMBER \ 10
    MAX_NUMBER = 10
  Else
    preMaxNumber = 0
    preNumber = ""
  End If
  maxCount = ThisDocument.Paragraphs.Count
  For i = 1 To maxCount
    If haveZero Then
      Do
        A = Int(Rnd(Timer) * MAX_NUMBER)
      Loop Until A > 0
    Else
      A = Int(Rnd(Timer) * MAX_NUMBER)
    End If
    
    FU = Rnd(Timer)
    If FU > 0.5 Then
      If haveZero Then
        Do
          B = Round(Rnd(Timer) * (MAX_NUMBER - A))
        Loop Until B > 0
      Else
        B = Round(Rnd(Timer) * (MAX_NUMBER - A))
      End If
      If styleExt Then '允许扩展样式
        If Rnd(Timer) > 0.5 Then
          out = "(   ) － B ＝ A"
        Else
          out = "A ＋ B ＝ (   )"
        End If
      Else
        out = "A ＋ B ＝ "
      End If
    Else
      If A < MAX_NUMBER \ 2 Then A = A + MAX_NUMBER \ 2
      If haveZero Then
        Do
          B = Round(Rnd(Timer) * A)
        Loop Until B > 0
      Else
        B = Round(Rnd(Timer) * A)
      End If
      If styleExt Then '允许扩展样式
        If Rnd(Timer) > 0.7 Then
          out = "A － (   ) ＝ B"
        Else
          If Rnd(Timer) > 0.5 Then
            out = "(   ) ＋ B ＝ A"
          Else
            out = "A － B ＝ (   )"
          End If
        End If
      Else
        out = "A － B ＝ "
      End If
    End If
    If preMaxNumber > 0 Then
      preNumber = CStr(Int(Rnd(Timer) * preMaxNumber))
      If preNumber = "0" Then preNumber = ""
    End If
    out = Replace(out, "A", Right("  " & preNumber & A, 2))
    out = Replace(out, "B", Right("  " & B, 2))
    If i < maxCount Then out = out & vbCrLf
    Set p = ThisDocument.Paragraphs(i)
    p.Range.Text = out
  Next
  Me.Hide
End Sub

Public Sub 算术式生成()
  MainForm.Show vbModal
End Sub
